<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>
<jsp:include page="../../includes/pages/include_header.jsp"></jsp:include>

<script>
$(document).ready(function() { 
	$(function(){
	   $("#submit").click(function(e) {
			e.preventDefault();
			$.get("../../SearchEngineServlet?action=search&searchText=" + $('#searchText').val(),function(responseText) {
				$('#searchOutput').html(responseText);
		 	});
	   });
	});
	
	$(function(){
  		$("#reset").click(function(e) {
			e.preventDefault();
			$('#searchOutput').html("");
			$('#searchText').val("");
	  	});
	});
});
 </script>

<h1>Focused crawler: Data mining</h1>
<div class="form">
	<form name="search" method="post" action="#">
    	<table class="table_form">
    		<tr class="required">
				<td class="label">
                	Search for:
           		</td>
            	<td class="content">
                	<input type="text" name="searchText" id="searchText" size="30" />
                	<input type="submit" name="submit" id="submit" value="Search" />
                	<input type="reset" name="reset" id="reset" value="Reset" />
				</td>
            </tr>
    	</table>
	</form>
</div>

<div id="searchOutput"></div>


<jsp:include page="../../includes/pages/include_footer.jsp"></jsp:include>