Link - A Focussed crawler
---------------------------
This is an implementation of a focussed crawler that searches the DBLP database
for pdf's about data-mining.

Using the system:
---------------------------
For now we lack a stand-alone executable.
Easiest is to import the project into eclipse and run it from there:
- Have a standard Glassfish 4 server installed and running
- Should be accessible from Eclipse
- Run on server

You can train the classifier using the TrainClassifier class. Just run it first, before using the system.

The code can be found on
https://bitbucket.org/kwyckmans/link-a-focused-crawler

Requirements
---------------------------
Our system uses JRE1.7, which is required. 
On top of this the Glassfish 4 server should be installed.
All other dependencies are included in the project and should not pose a problem.

Configuring the crawler:
---------------------------
We did not have the time to process commandline arguments, so link an initial seed has 
to be provided in the CrawlerMain.java file 

Searching results:
---------------------------
Once the crawler is done running, you can visit
localhost:8080/link-a-focused-crawler/ (or whatever address/port you used for glassfish)
to search the database.
