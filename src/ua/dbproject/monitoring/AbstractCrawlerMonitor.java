package ua.dbproject.monitoring;

public abstract class AbstractCrawlerMonitor implements Runnable{
	public abstract void setQueueSize(long l);

	public abstract void setAssignedPages(long numberOfAssignedPages);

	public abstract void setProcessedPages(long numberOfProcessedPages);

	public abstract void incProcessedPdf();

}
