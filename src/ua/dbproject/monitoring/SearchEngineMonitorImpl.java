package ua.dbproject.monitoring;

public class SearchEngineMonitorImpl extends AbstractSearchEngineMonitor {

  // SearchEngineMonitorImpl should be a singleton.
  // Only one instance can be obtained via the getInstance() method.
  private static SearchEngineMonitorImpl searchEngineMonitorInstance = null;

  private SearchEngineMonitorImpl() {
    // ... to be implemented
  }

  public static SearchEngineMonitorImpl getInstance() {
    if (searchEngineMonitorInstance == null) {
      searchEngineMonitorInstance = new SearchEngineMonitorImpl();
    }
    return searchEngineMonitorInstance;
  }

}
