package ua.dbproject.monitoring;

public class CrawlerMonitorImpl extends AbstractCrawlerMonitor {
	private long queueSize;
	private long processedPages;
	private long assignedPages;
	private long processedPdf = 0;
	private long processedXml;
	
	// CrawlerMonitorImpl should be a singleton.
	// Only one instance can be obtained via the getInstance() method.
	private static CrawlerMonitorImpl crawlerMonitorInstance = null;

	private CrawlerMonitorImpl() {
		// ... to be implemented
	}
	
	public void setQueueSize(long l){
		queueSize = l;
	}

	public static CrawlerMonitorImpl getInstance() {
		if (crawlerMonitorInstance == null) {
			crawlerMonitorInstance = new CrawlerMonitorImpl();
		}
		return crawlerMonitorInstance;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		while(true){
			System.out.println("Size of the Queue: " + queueSize);
			System.out.println("Total number of processed pages: " + processedPages);
			System.out.println("Number of pdf files classified: " + processedPdf);
			try { 
				Thread.sleep(60000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void setAssignedPages(long numberOfAssignedPages) {
		assignedPages = numberOfAssignedPages;
	}

	@Override
	public void setProcessedPages(long numberOfProcessedPages) {
	//	System.out.println("Updating number processed pages " + numberOfProcessedPages);
		processedPages = numberOfProcessedPages;
	}

	@Override
	public void incProcessedPdf(){
		processedPdf += 1;
	}
}
