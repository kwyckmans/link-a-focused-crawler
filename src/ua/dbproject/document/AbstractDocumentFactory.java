package ua.dbproject.document;

public abstract class AbstractDocumentFactory {

  // Produces a new instance of type AbstractDocument.
  public abstract AbstractDocument createDocument();

}
