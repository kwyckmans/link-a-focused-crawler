package ua.dbproject.document;

import edu.uci.ics.crawler4j.crawler.Page;
import ua.dbproject.classifier.AbstractCategory;
import ua.dbproject.queuing.AbstractQueueItem;

public abstract class AbstractDocument extends AbstractQueueItem {
	
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
// An AbstractDocument at least has one AbstractCategory.
  protected AbstractCategory category;

  // Added for Crawler4J Integration
  public abstract void setPage(Page page);
  
  public abstract Page getPage();

  public abstract void setContents(String parsePDFInputStream);
  
  public abstract String getContents();
  
  public abstract void setStemmedContents(String[] pdfStemmed);
  
  public abstract String getStemmedContents();
  
  
}
