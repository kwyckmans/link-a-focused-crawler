package ua.dbproject.document;

import edu.uci.ics.crawler4j.crawler.Page;

public class Document extends AbstractDocument {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4020311697680984701L;
	private Page page;
	private String contents;
	private String[] stemmedContents;
	
	public void setPage(Page p){
		this.page = p;
	}
	
	public void setContents(String cont){
		this.contents = cont;
	}
	
	public Page getPage(){
		return page;
	}
	
	public String getContents(){
		return contents;
	}
	
	public void setStemmedContents(String[] pdfStemmed) {
		this.stemmedContents = pdfStemmed;
	}
	
	// Return stemmed data as string
	public String getStemmedContents() {
		String result = "";
		for (int i = 0; i < stemmedContents.length; i++) {
			result += stemmedContents[i];
			result += " ";
		}
		return result;
	}
	
}
