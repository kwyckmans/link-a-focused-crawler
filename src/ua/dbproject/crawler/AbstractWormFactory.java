package ua.dbproject.crawler;

public abstract class AbstractWormFactory {

  // Produces a new instance of type  AbstractWorm.
  public abstract AbstractWorm createWorm();

}
