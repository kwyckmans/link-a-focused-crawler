package ua.dbproject.crawler;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;

import ua.dbproject.classifier.AbstractClassifierFactory;
import ua.dbproject.classifier.ClassifierFactory;
import ua.dbproject.monitoring.AbstractCrawlerMonitor;
import ua.dbproject.monitoring.CrawlerMonitorImpl;
import ua.dbproject.queuing.AbstractPriorityQueue;
import ua.dbproject.queuing.PriorityQueueImpl;
import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.frontier.Frontier;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;
import edu.uci.ics.crawler4j.url.WebURL;
import edu.uci.ics.crawler4j.util.IO;

public class CrawlerImpl extends AbstractCrawler {
	static final Logger logger = Logger.getLogger(CrawlerImpl.class.getName());
	//private PriorityQueueImpl frontier;

  private CrawlerImpl(CrawlConfig config, PageFetcher pageFetcher,
			RobotstxtServer robotstxtServer) throws Exception {
		super(config, pageFetcher, robotstxtServer);
		boolean resumable = config.isResumableCrawling();

        EnvironmentConfig envConfig = new EnvironmentConfig();
        envConfig.setAllowCreate(true);
        envConfig.setTransactional(resumable);
        envConfig.setLocking(resumable);

        File envHome = new File(config.getCrawlStorageFolder() + "/frontier");
        if (!envHome.exists()) {
                if (!envHome.mkdir()) {
                        throw new Exception("Couldn't create this folder: " + envHome.getAbsolutePath());
                }
        }
        if (!resumable) {
                IO.deleteFolderContents(envHome);
        }
		Environment env = new Environment(envHome, envConfig);
		System.out.println("Assigning PriorityQueue");
		Frontier crawlerQueue = new PriorityQueueImpl(env, config, docIdServer);
		
		this.setFrontier(crawlerQueue);
		
		classifierFactory = new ClassifierFactory();
	}

  // CrawlerImpl should be a singleton.
  // Only one instance can be obtained via the getInstance() method.
  private static CrawlerImpl crawlerInstance = null;
  
  protected AbstractCrawlerMonitor crawlerMonitor;

  public static CrawlerImpl getInstance(CrawlConfig config, PageFetcher pageFetcher, RobotstxtServer robotstxtServer) throws Exception {
    if (crawlerInstance == null) {
      crawlerInstance = new CrawlerImpl(config, pageFetcher, robotstxtServer);
    }
    return crawlerInstance;
  }

  public void initialize(AbstractCrawlerMonitor monitor){
	  this.crawlerMonitor = monitor;
  }

  /**
   * Start crawling given a seed url
   * TODO Add parameter for number of worms
   */
  public void start(String url){
    System.out.println("START CRAWLING: " + url);    
    List<String> seeds = generateStartUrls(url);
    System.out.println(seeds.size());
    List<String> subseeds = seeds.subList(0, 1);
    for(String seed : subseeds){
    	logger.debug(seed);
    	this.addSeed(seed);
    }
    
    this.startNonBlocking(Worm.class, 30);
    (new Thread(crawlerMonitor)).start();
  }

  /**
   * Methdo specific to crawling DBLP starting with the http://dblp.uni-trier.de/db/indices/AUTHORS URL
   * @param url
   * @return
   */
	private List<String> generateStartUrls(String url) {
		System.out.println("Generating author urls");
		if (!url.equals("http://dblp.uni-trier.de/db/indices/AUTHORS")) {
			List<String> list = new ArrayList<String>();
			list.add(url);
			return list;
		}
		URL seed;
		List<String> list = new ArrayList<String>();
		try {
			seed = new URL(url);
			InputStream is = seed.openStream();  // throws an IOException
	        BufferedReader br = new BufferedReader(new InputStreamReader(is));
	        String line;
	        while ((line = br.readLine()) != null) {
	        	//System.out.println(line);
	        	String result;
	        	result = line.replace("'", "=");
	        	result = result.replace(".", "=");
	        	String lName = result.substring(result.lastIndexOf(" ") + 1);
	        	//lName = lName.replace("'", "=");
	        	//System.out.println("lName: " + lName);
	        	
	        	char letter = Character.toLowerCase(lName.charAt(0));
	        	
	        	String fName = result.substring(0, result.lastIndexOf(" ")).replace(" ", "_");
	        	//System.out.println("fName: " + fName);
	        	String curl = "http://dblp.uni-trier.de/pers/xk/"+ letter +"/"+ lName +':'+ fName;
	        	list.add(curl);
	        }
	        
	        return list;
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        

		return null;
	}
  
  public void stop() {
	  this.stop();
  }

	@Override
	public AbstractClassifierFactory getClassfierFactory() {
		// TODO Auto-generated method stub
		return new ClassifierFactory();
	}
	
	public AbstractCrawlerMonitor getMonitor(){
		return crawlerMonitor;
	}
}
