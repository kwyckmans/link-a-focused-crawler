package ua.dbproject.crawler;
/*
import ua.dbproject.document.AbstractDocument;
import ua.dbproject.document.DocumentFactory;
import ua.dbproject.indexer.AbstractIndexManager;
import ua.dbproject.indexer.IndexManagerFactory;
*/

import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import ua.dbproject.document.AbstractDocument;
import ua.dbproject.document.DocumentFactory;
import ua.dbproject.indexer.AbstractIndexManager;
import ua.dbproject.indexer.IndexManagerFactory;
import ua.dbproject.indexer.IndexManagerImpl;
import ua.dbproject.queuing.AbstractPriorityQueue;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.frontier.Frontier;
import edu.uci.ics.crawler4j.url.WebURL;

public class Worm extends AbstractWorm{
	static final Logger logger = Logger.getLogger(Worm.class.getName());
	
	private final static Pattern FILTERS = Pattern.compile(".*(\\.(css\\?c?|js|css|bmp|gif|ico|jpe?g|dtd|php|asp" + "|png|tiff?|mid|mp2|mp3|mp4"
            + "|wav|avi|mov|mpeg|ram|m4v" + "|rm|smil|wmv|swf|wma|zip|rar|gz))$");
	
	@Override
	/**
	 * Determines if a url on a processed page should be visited or not. 
	 * TODO shouldVisit Decision should be made in indexer
	 * TODO Add list of publicist websites that should be skipped
	 */
	public boolean shouldVisit(WebURL url){
		String href = url.getURL().toLowerCase();
		logger.debug("ShouldVisit TEST: " + href + " " + !FILTERS.matcher(href).matches());
		logger.debug(url);
		// We only want .htm(l) and .pdf files.
		return !FILTERS.matcher(href).matches() && !IndexManagerImpl.isPublisher(url.getDomain(), url.getParentUrl().toString());
	}
	
	@Override
	public void onContentFetchError(WebURL webUrl){
		System.out.println("Content error! " + webUrl.toString());
	}
	
	@Override
	public void onParseError(WebURL webUrl){
		System.out.println("Parse error! " + webUrl.toString());
	}
	
	@Override
	/**
	 * Process next page in Queue
	 */
	public void visit(Page page){
		DocumentFactory docFac = new DocumentFactory();
		AbstractDocument doc = docFac.createDocument();
		
		doc.setPage(page);
		IndexManagerFactory absFac = new IndexManagerFactory();
		AbstractIndexManager indexer = absFac.createIndexManager();
		
		indexer.processDocument(doc, this.getMyController());
		Frontier frontier = this.getMyController().getFrontier();
		/*
		 * These do not work..
		 */
		//((CrawlerImpl) this.getMyController()).getMonitor().setAssignedPages(this.getMyController().getFrontier().getNumberOfAssignedPages());
		((CrawlerImpl) this.getMyController()).getMonitor().setQueueSize(((AbstractPriorityQueue)frontier).getScheduledPages());
		((CrawlerImpl) this.getMyController()).getMonitor().setProcessedPages(this.getMyController().getFrontier().getNumberOfProcessedPages());
	}
}
