package ua.dbproject.crawler;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;
import ua.dbproject.classifier.AbstractClassifierFactory;
import ua.dbproject.indexer.AbstractIndexManagerFactory;
import ua.dbproject.monitoring.AbstractCrawlerMonitor;
import ua.dbproject.storage.AbstractStorageManagerFactory;

public abstract class AbstractCrawler extends CrawlController {

  public AbstractCrawler(CrawlConfig config, PageFetcher pageFetcher,
			RobotstxtServer robotstxtServer) throws Exception {
		super(config, pageFetcher, robotstxtServer);
		// TODO Auto-generated constructor stub
	}

  protected AbstractClassifierFactory classifierFactory;

  protected AbstractIndexManagerFactory indexManagerFactory;

  protected AbstractStorageManagerFactory storageManagerFactory;

  // Creates Worms that extend Crawler4J WebCrawlers
  protected AbstractWormFactory wormFactory;
 
  // Frontier from Crawler4J
  protected AbstractCrawlerQueue crawlerQueue;

  protected AbstractCrawlerMonitor crawlerMonitor;

  // Initializes the Crawler with a given AbstractCrawlerMonitor object.
  public void initialize(AbstractCrawlerMonitor monitor) {
    this.crawlerMonitor = monitor;
  }

  // Starts crawling from a given URL.
  public abstract void start(String url);

  // Stops crawling.
  public abstract void stop();
  
  public abstract AbstractClassifierFactory getClassfierFactory();
}
