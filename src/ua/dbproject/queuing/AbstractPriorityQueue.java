package ua.dbproject.queuing;

import java.util.List;

import com.sleepycat.je.Environment;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.frontier.DocIDServer;
import edu.uci.ics.crawler4j.frontier.Frontier;
import edu.uci.ics.crawler4j.url.WebURL;

public abstract class AbstractPriorityQueue extends Frontier {

  public AbstractPriorityQueue(Environment arg0, CrawlConfig arg1,
			DocIDServer arg2) {
		super(arg0, arg1, arg2);
		// TODO Auto-generated constructor stub
	}

// Adds an item to the queue. 
  // This method should be thread-safe.
  public abstract void enqueue(AbstractQueueItem queueItem);

  // Peeks at the first item of the queue without polling it.
  // This method should be thread-safe.
  public abstract AbstractQueueItem peek();

  // Polls the first item of the queue.
  // This method should be thread-safe.
  public abstract AbstractQueueItem poll();
  
  @Override
  public void scheduleAll(List<WebURL> urls){
	  super.scheduleAll(urls);
  }
  
  @Override
	public void schedule(WebURL url) {
		// TODO Auto-generated method stub
		super.schedule(url);
	}

  public abstract long getScheduledPages();

}
