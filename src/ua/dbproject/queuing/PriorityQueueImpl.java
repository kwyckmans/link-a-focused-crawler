package ua.dbproject.queuing;

import java.util.List;

import org.apache.log4j.Logger;

import ua.dbproject.crawler.Worm;

import com.sleepycat.je.Environment;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.frontier.DocIDServer;
import edu.uci.ics.crawler4j.frontier.Counters.ReservedCounterNames;
import edu.uci.ics.crawler4j.url.WebURL;

public class PriorityQueueImpl extends AbstractPriorityQueue {
	static final Logger logger = Logger.getLogger(PriorityQueueImpl.class.getName());

	public PriorityQueueImpl(Environment arg0, CrawlConfig arg1,
			DocIDServer arg2) {
		super(arg0, arg1, arg2);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void enqueue(AbstractQueueItem queueItem) {
		// TODO Auto-generated method stub

	}

	@Override
	public void scheduleAll(List<WebURL> urls){
		logger.debug("Add multiple urls at once");
		//System.out.println("Added following urls:");
		for(WebURL url : urls){
			logger.debug(url.toString());
			//System.out.println(url.toString());
			//System.out.println(url.toString());
		}
		super.scheduleAll(urls);
	}
	
	@Override
	public long getScheduledPages(){
		return this.counters.getValue(ReservedCounterNames.SCHEDULED_PAGES);
	}
	
	@Override
	public void schedule(WebURL url) {
		// TODO Auto-generated method stub
		logger.debug("Added url to queue");
		logger.debug(url.toString());
		//System.out.println("Added: " + url.toString());
		super.schedule(url);
	}
	
	@Override
	public AbstractQueueItem peek() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractQueueItem poll() {
		// TODO Auto-generated method stub
		return null;
	}

}
