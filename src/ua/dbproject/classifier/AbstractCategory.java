package ua.dbproject.classifier;

public abstract class AbstractCategory {

  // Holds a label for this category.
  public abstract String getLabel();
  
}
