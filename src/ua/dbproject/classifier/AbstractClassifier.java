package ua.dbproject.classifier;

import java.io.IOException;
import java.text.ParseException;

import jnisvmlight.SVMLightModel;
import ua.dbproject.document.AbstractDocument;

public abstract class AbstractClassifier {

  // Check out the Java Native Interface (JNI) for SVM-light from:
  // http://www.mpi-inf.mpg.de/~mtb/svmlight/JNI_SVM-light-6.01.zip
  protected SVMLightModel fModel;
  
  // Trains a classifier by positive and negative pdf files
  public abstract void trainClassifier(String[] positivePDFFiles, String[] negativePDFFiles) throws IOException, ParseException; 

  // Classifies a document by assigning one or more categories to it.
  public abstract void classifyDocument(AbstractDocument document) throws Exception;
  
  // Write a classifier (model and feature list) to file
  public abstract void writeToFile() throws IOException;
}
