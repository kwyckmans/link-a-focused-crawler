package ua.dbproject.classifier;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.ParseException;

import jnisvmlight.FeatureVector;
import jnisvmlight.SVMLightInterface;
import jnisvmlight.LabeledFeatureVector;
import jnisvmlight.SVMLightModel;
import jnisvmlight.TrainingParameters;

import ua.dbproject.document.AbstractDocument;
import ua.dbproject.storage.StorageManager;
import ua.dbproject.tools.stemmer.StemmerDriver;

public class Classifier extends AbstractClassifier {
	
	private FeatureParser fFeatureParser;
	private String fName;
	private StorageManager fStorageManager;
	
	// Make classifier with a specified name. If a classifier with the same name already exists, this classifier will import its data
	public Classifier(String name) throws Exception {
		
		// Set name
		fName = name;
		
		// Import data if classifier already exists
		File model = new File("./storage/classifier/" + fName + "_Model.dat");
		if (model.canRead()) {
			
			// Read feature list
			fFeatureParser = new FeatureParser(fName);
			
			// Read model
			fModel = SVMLightModel.readSVMLightModelFromURL(model.toURI().toURL());
		
		} else {
			
			// Make new feature list
			fFeatureParser = new FeatureParser();
		
		}
		
		// Initialize storage manager
		initializeStorage();
	}
	
	private void initializeStorage() throws Exception {
		File index = new File("./storage/lucene/" + fName + "-index/segments.gen");
		if (index.canRead()) {
			fStorageManager = new StorageManager("./storage/lucene/" + fName + "-index", false);
		} else {
			fStorageManager = new StorageManager("./storage/lucene/" + fName + "-index", true);
		}
	}
	
	// Write model and feature list to file 
	public void writeToFile() throws IOException {
		
		// Write model to file
		fModel.writeModelToFile("./storage/classifier/" + fName + "_Model.dat");
		
		// Write feature list to file
		fFeatureParser.writeToFile(fName);
	}
	
	public void classifyDocument(AbstractDocument document) throws Exception {
		
		// Extract pdf string from document
		String pdfString = document.getContents();
		
		// Stem string
		StemmerDriver stemmer = new StemmerDriver();
		String[] pdfStemmed = stemmer.stem(pdfString);
		document.setStemmedContents(pdfStemmed);
		
		// Generate feature vector
		FeatureVector classifyData = fFeatureParser.generateFeatureVector(pdfStemmed);
				
		// Classify 
		double result = fModel.classify(classifyData);
		if (result > 0) {
			fStorageManager.storeDocument(document);
		}
	}
	
	// Train the classifier with some preselected pdf files
	public void trainClassifier(String[] positivePDFFiles, String[] negativePDFFiles) throws IOException, ParseException {
		
		// Train the feature parser
		LabeledFeatureVector[] trainData = fFeatureParser.generateTrainData(positivePDFFiles, negativePDFFiles);
	
		// Initialize training parameters
	    TrainingParameters trainParams = new TrainingParameters();
	    // trainParams.getLearningParameters().verbosity = 1;
	    
	    // Train model
	    SVMLightInterface trainer = new SVMLightInterface();
	    fModel = trainer.trainModel(trainData, trainParams);
	}
}
