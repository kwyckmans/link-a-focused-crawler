package ua.dbproject.classifier;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jnisvmlight.FeatureVector;
import jnisvmlight.LabeledFeatureVector;

import ua.dbproject.tools.pdfconverter.PDFConverter;
import ua.dbproject.tools.stemmer.StemmerDriver;

public class FeatureParser {
	
	private final static int MAX_FEATURES = 9000;
	private FeatureList fFeatureList;
	private PDFConverter fPDFConverter;
	private StemmerDriver fStemmer;
	
	// Make new feature parser with empty feature list
	public FeatureParser() {
		fFeatureList = new FeatureList();
		fPDFConverter = new PDFConverter();
		fStemmer = new StemmerDriver();
	}
	
	// Make new feature parser with existing feature list
	public FeatureParser(String listName) throws IOException {
		fFeatureList = new FeatureList(listName);
		fPDFConverter = new PDFConverter();
		fStemmer = new StemmerDriver();
	}
	
	// Write feature list to file
	public void writeToFile(String fileName) throws IOException {
		fFeatureList.writeToFile(fileName);
	}
	
	public LabeledFeatureVector[] generateTrainData(String[] positivePDFFiles, String[] negativePDFFiles) throws IOException {
		
		// Make new array of vectors
		int totalSize = positivePDFFiles.length + negativePDFFiles.length;
		LabeledFeatureVector[] result = new LabeledFeatureVector[totalSize]; 
		
		// Add positive vectors
		int totalIndex = 0;
		for (int p = 0; p < positivePDFFiles.length; p++) {
			result[totalIndex] = generateLabeledFeatureVector(positivePDFFiles[p], true);
			totalIndex++;
		}
		
		// Add negative vectors
		for (int n = 0; n < negativePDFFiles.length; n++) {
			result[totalIndex] = generateLabeledFeatureVector(negativePDFFiles[n], false);
			totalIndex++;
		}
		
		// Return result
		return result;
	}
	
	public FeatureVector generateFeatureVector(String[] pdfStemmed) {
	
		// Make feature vector by scanning words
		Integer[] totalVector = new Integer[MAX_FEATURES];
		int nrFeaturesUsed = 0;
		for(int i = 0; i < pdfStemmed.length; i++) {
			
			// Get index number
			int index = fFeatureList.getIndex(pdfStemmed[i]);
			
			// Check whether feature exists in the feature list
			if (index != -1) {
			
				// Update occurence value
				if (totalVector[index] == null) {
					totalVector[index] = 1;
					nrFeaturesUsed++;
				} else {
					totalVector[index]++;
				}
			}
		}
		
		// Generate feature vector
		int[] dims = new int[nrFeaturesUsed];
		double[] vals = new double[nrFeaturesUsed];
		int featureIndex = 0;
		for (int j = 0; j < MAX_FEATURES; j++) {
			if (totalVector[j] != null) {
				dims[featureIndex] = j + 1;
				vals[featureIndex] = totalVector[j];
				featureIndex++;
			}
		}
		FeatureVector result = new FeatureVector(dims, vals);
		
		// Return result
		return result;
	}
	
	private LabeledFeatureVector generateLabeledFeatureVector(String fileName, boolean positive) throws FileNotFoundException {
		
		// Read file
		File file = new File("./traindata/" + fileName + ".pdf");
		InputStream pdfFile = new FileInputStream(file);
		
		// Concert pdf
		String pdfString = fPDFConverter.parsePDFInputStream(pdfFile);
		
		// Stem file
		String[] pdfStemmed = fStemmer.stem(pdfString);
		
		// Make feature vector by scanning words
		Integer[] totalVector = new Integer[MAX_FEATURES];
		int nrFeaturesUsed = 0;
		for(int i = 0; i < pdfStemmed.length; i++) {
			
			// Get index number
			int index = fFeatureList.getOrInsertIndex(pdfStemmed[i]);
			
			// Update occurence value
			if (totalVector[index] == null) {
				totalVector[index] = 1;
				nrFeaturesUsed++;
			} else {
				totalVector[index]++;
			}
		}
		
		// Generate labeled feature vector
		int[] dims = new int[nrFeaturesUsed];
		double[] vals = new double[nrFeaturesUsed];
		int featureIndex = 0;
		for (int j = 0; j < MAX_FEATURES; j++) {
			if (totalVector[j] != null) {
				dims[featureIndex] = j + 1;
				vals[featureIndex] = totalVector[j];
				featureIndex++;
			}
		}
		double label = 0;
		if (positive) {
			label = 1;
		} else {
			label = -1;
		}
		LabeledFeatureVector result = new LabeledFeatureVector(label, dims, vals);
		
		// Return result
		return result;
	}

}
