package ua.dbproject.classifier;

import java.io.IOException;
import java.text.ParseException;

public abstract class AbstractClassifierFactory {
  
  // Procudes an instance of type AbstractClassifier 
  public abstract AbstractClassifier createClassifier(String name) throws Exception;

}
