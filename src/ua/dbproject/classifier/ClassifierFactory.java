package ua.dbproject.classifier;

import java.io.IOException;
import java.text.ParseException;

public class ClassifierFactory extends AbstractClassifierFactory {

	public AbstractClassifier createClassifier(String fileName) throws Exception {
		return new Classifier(fileName);
	}

}
