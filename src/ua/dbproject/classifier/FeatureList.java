package ua.dbproject.classifier;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class FeatureList {

	private ArrayList<String> fFeatures;
	
	// Make new empty feature list
	public FeatureList() {
		fFeatures = new ArrayList<String>();
	}
	
	// Make feature list from txt file
	public FeatureList(String listName) throws IOException {
		readFromFile(listName);
	}
	
	// Write the features to a txt file (one word/feature per line)
	public void writeToFile(String listName) throws IOException {
	
		// Make new file
		FileWriter file = new FileWriter("./storage/classifier/" + listName + "_FeatureList.txt");
		PrintWriter output = new PrintWriter(file);
			
		// Write words to file
		for (int i = 0; i < fFeatures.size(); i++) {
			output.println(fFeatures.get(i));
		}
		
		// Close output file
		output.close();
	}
	
	// Read feature list from a txt file (one word/feature per line)
	private void readFromFile(String listName) throws IOException {
		
		// Make new feature list
		fFeatures = new ArrayList<String>();
	
		// Read file and process words
		File file = new File("./storage/classifier/" + listName + "_FeatureList.txt");
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line;
		while ((line = br.readLine()) != null) {
			fFeatures.add(line);
		}
		br.close();
	}
	
	// Returns the index of a feature, inserts the feature if it does not already exist
	public int getOrInsertIndex(String feature) {
		
		// Check whether feature already exists
		boolean found = false;
		int index = 0;
		while ((!found) && (index < fFeatures.size())) {
			if (fFeatures.get(index).equals(feature)) {
				found = true;
			} else {
				index++;
			}
		}
		
		// Insert feature if it does not exist
		if (!found) {
			fFeatures.add(index, feature);
		}
		
		// Return index
		return index;
	}
	
	// Returns the index of a feature, returns -1 if the feature does not exist
	public int getIndex(String feature) {
		
		// Check whether feature already exists
		for (int index = 0; index < fFeatures.size(); index++) {
			if (fFeatures.get(index).equals(feature)) {
				return index;
			}
		}
		
		// Return zero of the index does not exist
		return -1;		
	}

}
