package ua.dbproject.indexer;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Hashtable;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.Logger;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.url.WebURL;
import ua.dbproject.classifier.AbstractClassifier;
import ua.dbproject.classifier.AbstractClassifierFactory;
import ua.dbproject.document.AbstractDocument;
import ua.dbproject.tools.dblpxmlparser.DblpXMLParser;
import ua.dbproject.tools.pdfconverter.PDFConverter;
import ua.dbproject.crawler.CrawlerImpl;

public class IndexManagerImpl extends AbstractIndexManager {
	private static File storageFolder;
	static final Logger logger = Logger.getLogger(IndexManagerImpl.class.getName());
	
	private final static Hashtable<WebURL, String> publishers = new Hashtable<>();
	
	static
	{
		WebURL curl = new WebURL();
		curl.setURL("http://link.springer.com/");
		publishers.put(curl, "springer");
		curl.setURL("http://www.sciencedirect.com/");
		publishers.put(curl, "sciencedirect");
		curl.setURL("http://www.elsevier.com/");
		publishers.put(curl, "elsevier");
		curl.setURL("http://dl.acm.org/");
		publishers.put(curl, "acm");
		curl.setURL("http://www.pubzone.org/");
		publishers.put(curl, "pubzone");
		/*curl.setURL("http://dx.doi.org/");
		publishers.put(curl, "springer");*/
	}
	
	public static void configure(String[] domain, String storageFolderName) {
       // ImageCrawler.crawlDomains = domain;

        storageFolder = new File(storageFolderName);
        if (!storageFolder.exists()) {
                storageFolder.mkdirs();
        }
	}
	
	public static boolean isPublisher(String domain, String parentURL){
		return publishers.containsValue(domain) || publishers.containsKey(parentURL);	
	}
	
	@Override
	public void processDocument(AbstractDocument document, CrawlController crawlController) {
		String url = document.getPage().getWebURL().getURL();
		logger.debug("Processing: " + url);
		logger.debug("Content type: " + document.getPage().getContentType());
		
		if(document.getPage().getContentType().equals("application/pdf")){
			PDFConverter pdf = new PDFConverter();

			document.setContents(pdf.parsePDFInputStream(new ByteArrayInputStream(document.getPage().getContentData())));
			//IO.writeBytesToFile(document.getPage().getContentData(), storageFolder.getAbsolutePath() + "/" + name);

			try {
				AbstractClassifierFactory classifierFactory = ((CrawlerImpl)crawlController).getClassfierFactory(); 
				AbstractClassifier classifier =  classifierFactory.createClassifier("MainClassifier");
				classifier.classifyDocument(document);
				((CrawlerImpl) crawlController).getMonitor().incProcessedPdf();
			} catch (Exception e) {
				System.out.println("Something went wrong when encountering a pdf");
				((CrawlerImpl) crawlController).getMonitor().incProcessedPdf();
			}
		}
		
		if(document.getPage().getContentType().equals("text/xml")){
			SAXParserFactory factory = SAXParserFactory.newInstance();
			DblpXMLParser handler = new DblpXMLParser();
			SAXParser saxParser;
			try {
				saxParser = factory.newSAXParser();
				saxParser.parse(new InputSource(new ByteArrayInputStream(document.getPage().getContentData())), handler);
				List<WebURL> links = handler.getLinks();
				crawlController.getFrontier().scheduleAll(links);
			} catch (ParserConfigurationException | SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
		}
	}

}
