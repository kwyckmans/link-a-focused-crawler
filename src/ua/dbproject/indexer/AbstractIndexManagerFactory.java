package ua.dbproject.indexer;

public abstract class AbstractIndexManagerFactory {

  // Produces a new instance of type AbstractIndexManager. 
  public abstract AbstractIndexManager createIndexManager();

}
