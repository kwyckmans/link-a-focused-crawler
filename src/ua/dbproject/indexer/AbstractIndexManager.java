package ua.dbproject.indexer;

import edu.uci.ics.crawler4j.crawler.CrawlController;
import ua.dbproject.document.AbstractDocument;
import ua.dbproject.tools.htmlparser.HTMLParser;
import ua.dbproject.tools.pdfconverter.PDFConverter;
import ua.dbproject.tools.stemmer.StemmerDriver;

public abstract class AbstractIndexManager {

  protected PDFConverter pdfConverter;

  protected StemmerDriver stemmerDriver;

  protected HTMLParser parser;
  
  // Processes a document. All the parsing should take place here.
  public abstract void processDocument(AbstractDocument document, CrawlController crawlController);
}
