package ua.dbproject.tools.htmlparser;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Stack;

import javax.swing.text.BadLocationException;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.parser.ParserDelegator;

// Implement your own HTML callback methods here.
public class HTMLParser extends HTMLEditorKit.ParserCallback {

  protected Stack<HTML.Tag> stack = new Stack<HTML.Tag>();

  public void flush() throws BadLocationException {
  }

  public void handleComment(char[] data, int pos) {
  }

  public void handleStartTag(HTML.Tag tag, MutableAttributeSet a, int pos) {
    stack.push(tag);
  }

  public void handleEndTag(HTML.Tag tag, int pos) {
    if (!stack.empty())
      stack.pop();
  }

  public void handleSimpleTag(HTML.Tag tag, MutableAttributeSet a, int pos) {
  }

  public void handleError(String errorMsg, int pos) {
  }

  public void handleEndOfLineString(String eol) {
  }

  public void handleText(char[] data, int pos) {
    HTML.Tag tag = null;
    if (!stack.empty())
      tag = stack.peek();

    StringBuffer text = new StringBuffer();
    for (char ch : data)
      text.append(ch);

    System.out.println("TAG:" + tag + "\tTEXT: " + text);
  }

  public static void main(String[] args) throws Exception {
    BufferedReader reader = new BufferedReader(new InputStreamReader(new URL(args[0]).openStream(), "UTF-8"));
    HTMLParser parser = new HTMLParser();
    new ParserDelegator().parse(reader, parser, true);
    reader.close();
  }
}
