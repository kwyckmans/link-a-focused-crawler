package ua.dbproject.tools.stemmer;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.regex.Pattern;

import net.sf.snowball.SnowballProgram;

// Tokenizes and stems an input string into an array of stemmed strings (stopwords are removed).
public class StemmerDriver {

  public static int MIN_TERMLENGTH = 1;
  public static boolean STRICT_ALPHANUMERICAL = false;
  public static boolean MATCH_ALPHABET_AND_DIGITS_ONLY = false;
  public static boolean SPLIT_ALPHABET_AND_DIGITS = false;
  public static boolean SPLIT_COMPOSITE = true;
  public static boolean EXPAND_COMPOSITE = false;

  public static String concat(String[] data) {
    if (data == null || data.length == 0)
      return "";
    String s = data[0];
    for (int i = 1; i < data.length; i++) {
      s += " " + data[i];
    }
    return s;
  }

  public static String prepareString(String tmp) {
    tmp = tmp.trim();
    tmp = tmp.replaceAll("�", "ae");
    tmp = tmp.replaceAll("�", "ue");
    tmp = tmp.replaceAll("�", "oe");
    tmp = tmp.replaceAll("�", "ss");
    tmp = tmp.replaceAll("\\s", " ");
    return tmp;
  }

  private Pattern lettersAndDigitsPattern;

  private Class<?> stemClass = null;

  private SnowballProgram stemmer;

  private String stemmerLanguage = "english";

  private Method stemMethod;

  private String stopWordFile = "ua/dbproject/tools/stemmer/stopwords.txt";

  public HashSet<String> stopwords = null;

  public StemmerDriver() {
    this.lettersAndDigitsPattern = Pattern.compile("[0-9a-z]+");
    try {
      if (stemClass == null) {
        stemClass = Class.forName("net.sf.snowball.ext." + stemmerLanguage + "Stemmer");
      }
      stemmer = (SnowballProgram) (stemClass.newInstance());
      stemMethod = stemClass.getMethod("stem", new Class[0]);
      if (stopwords == null) {
        importStopwords();
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public StemmerDriver(String stemmerLanguage, String stopWordFile) {
    this();
    this.stemmerLanguage = stemmerLanguage;
    this.stopWordFile = stopWordFile;
  }

  public String[] dontStem(String data) {
    TextItem[] textItems = handleString(data, false);
    String[] stems = new String[textItems.length];
    for (int i = 0; i < textItems.length; i++) {
      stems[i] = textItems[i].stem;
    }
    return stems;
  }

  private int tokenOff;

  public synchronized TextItem[] handleString(String data, boolean doStem) {
    Vector<TextItem> textItems = new Vector<TextItem>();
    StringTokenizer tokenizer = new StringTokenizer(prepareString(data).toLowerCase(), " ", true);

    this.tokenOff = 0;
    int charOff = 0;
    while (tokenizer.hasMoreTokens()) {
      String token = tokenizer.nextToken();
      if (!token.equals(" ")) {
        Vector<TextItem> items = null;
        if (EXPAND_COMPOSITE) {
          String composite = token.replaceAll("(\\-|_|')", "");
          if (!token.equals(composite)) {
            int compositeOff = tokenOff;
            items = processString(composite, doStem);
            for (int i = 0; items != null && i < items.size(); i++) {
              TextItem item = items.get(i);
              item.characterOffset = item.characterOffset + charOff;
              textItems.add(item);
            }
            tokenOff = compositeOff;
          }
          items = processString(token, doStem);
        } else if (SPLIT_COMPOSITE) {
          items = processString(token.replaceAll("(\\-|_|')", " "), doStem);
        } else {
          items = processString(token.replaceAll("(\\-|_|')", ""), doStem);
        }
        for (int i = 0; items != null && i < items.size(); i++) {
          TextItem item = items.get(i);
          item.characterOffset = item.characterOffset + charOff;
          textItems.add(item);
        }
      }
      charOff += token.length();
    }

    TextItem[] allItems = new TextItem[textItems.size()];
    textItems.copyInto(allItems);
    return allItems;
  }

  private void importStopwords() throws Exception {
    BufferedReader buf = new BufferedReader(new InputStreamReader(this.getClass().getClassLoader().getResource(stopWordFile).openStream()));
    stopwords = new HashSet<String>();
    String tmp;
    while ((tmp = buf.readLine()) != null)
      stopwords.add(prepareString(tmp));
    buf.close();
  }

  public boolean isStopword(String s) {
    return (stopwords.contains(s));
  }

  private Vector<TextItem> processString(String token, boolean doStem) {
    Vector<TextItem> textItems = new Vector<TextItem>();

    char[] chars = token.toCharArray();

    boolean isDigit = false;
    boolean isLetter = false;
    boolean currentIsLetter = false;
    boolean currentIsDigit = false;

    TextItem item;
    String term;
    int off = 0;

    for (int i = 0; i <= chars.length && i <= chars.length; i++) {
      if (i < chars.length) {
        currentIsLetter = Character.isLetter(chars[i]);
        currentIsDigit = Character.isDigit(chars[i]);
      } else {
        currentIsLetter = false;
        currentIsDigit = false;
        isLetter = false;
        isDigit = false;
      }
      if ((!SPLIT_ALPHABET_AND_DIGITS && !currentIsDigit && !currentIsLetter)
          || (SPLIT_ALPHABET_AND_DIGITS && ((!currentIsDigit && !currentIsLetter) || (isDigit && !currentIsDigit) || (isLetter && !currentIsLetter)))) {

        // System.out.println(new String(chars, off, i - off));

        term = new String(chars, off, i - off);
        item = new TextItem(term, term, tokenOff, off);
        off = currentIsDigit || currentIsLetter ? i : i + 1;

        if (term.length() < MIN_TERMLENGTH || stopwords.contains(term) || (STRICT_ALPHANUMERICAL && !lettersAndDigitsPattern.matcher(term).matches()))
          continue;

        tokenOff++;

        if (doStem) {
          try {
            stemmer.setCurrent(term);
            stemMethod.invoke(stemmer, new Object[0]);
            item.stem = stemmer.getCurrent();
          } catch (Exception e) {
            e.printStackTrace();
          }
        }
        textItems.add(item);
      }
      isDigit = currentIsDigit;
      isLetter = currentIsLetter;
    }

    return textItems;
  }

  public String[] stem(char[] data) {
    return stem(new String(data));
  }

  public String[] stem(String data) {
    TextItem[] textItems = handleString(data, true);
    String[] stems = new String[textItems.length];
    for (int i = 0; i < textItems.length; i++)
      stems[i] = textItems[i].stem;
    return stems;
  }

  public String[] stem(String[] data) {
    return stem(concat(data));
  }

  public class TextItem {

    public int tokenOffset, characterOffset;

    public String token, stem;

    public TextItem(String token, String stem, int tokenOffset, int characterOffset) {
      this.token = token;
      this.stem = stem;
      this.tokenOffset = tokenOffset;
      this.characterOffset = characterOffset;
    }

    public String toString() {
      return stem + "[" + tokenOffset + "|" + characterOffset + "]";
    }
  }

  public static void main(String[] args) {
    String[] stemmedTokens = new StemmerDriver().stem(args[0]);
    for (String tok : stemmedTokens)
      System.out.println(tok);
  }
}