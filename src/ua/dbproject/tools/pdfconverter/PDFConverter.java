package ua.dbproject.tools.pdfconverter;

import java.io.FileInputStream;
import java.io.InputStream;

import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

//Extracts text from a PDF document and writes it into a string.
public class PDFConverter {

  public String parsePDFInputStream(InputStream inputStream) {

    String text = null;

    PDFParser parser;
    PDFTextStripper pdfStripper;

    PDDocument pdDoc = null;
    COSDocument cosDoc = null;

    try {

      parser = new PDFParser(inputStream);
      parser.parse();

      cosDoc = parser.getDocument();
      pdfStripper = new PDFTextStripper();
      pdDoc = new PDDocument(cosDoc);
      text = pdfStripper.getText(pdDoc);

    } catch (Exception e) {

      System.err.println("An exception occured in parsing the PDF stream.");
      e.printStackTrace();

      text = null;

    } finally {

      try {
        if (cosDoc != null)
          cosDoc.close();
        if (pdDoc != null)
          pdDoc.close();
      } catch (Exception e1) {
        e1.printStackTrace();
      }

    }

    return text;
  }

  public static void main(String args[]) throws Exception {
    System.out.println(new PDFConverter().parsePDFInputStream(new FileInputStream(args[0])));
  }
}
