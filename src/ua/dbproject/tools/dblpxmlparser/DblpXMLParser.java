package ua.dbproject.tools.dblpxmlparser;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.*;

import org.xml.sax.*;
import org.xml.sax.helpers.*;

import edu.uci.ics.crawler4j.url.WebURL;

public class DblpXMLParser extends DefaultHandler{
	private StringBuilder content;
	private List<WebURL> links;
	private boolean inWWW;
	private boolean inArticle;
	private boolean inProceedings;
	
	public DblpXMLParser(){
		links = new ArrayList<WebURL>();
		content = new StringBuilder();
	}
	
	public List<WebURL> getLinks(){
		return this.links;
	}
	
	public void characters(char[] buffer, int start, int length) {
        content.append(buffer, start, length);
	}


	public void startElement(String uri, String localName,
            String qName, Attributes attributes){ 
		content = new StringBuilder();
		if(qName.equalsIgnoreCase("www")){
			inWWW = true;
		}
		if(qName.equalsIgnoreCase("article")){
			inArticle = true;
		}
		if(qName.equalsIgnoreCase("inproceedings")){
			inProceedings = true;
		}
	}
	
	// TODO Add homonyms to links
	// TODO Add differnt type of links 
	public void endElement(String uri, String localName, String qName){
		//System.out.println(content.toString());
		if(qName.equalsIgnoreCase("dblpkey")){
			WebURL url = new WebURL();
			url.setURL("http://dblp.uni-trier.de/rec/bibtex/" + content.toString() + ".xml");
			//System.out.println(url.toString());
			links.add(url);
		}
		if(inWWW){
			if(qName.equalsIgnoreCase("url")){
				WebURL url = new WebURL();
				url.setURL(content.toString());
		//		System.out.println("inWWW: " + url.toString());
				links.add(url);
				inWWW = false;
			}
		}
		if(inProceedings){
			/*
			if(qName.equalsIgnoreCase("url")){
				WebURL url = new WebURL();
				url.setURL("http://dblp.uni-trier.de/" + content.toString());
				System.out.println("inProceedings: " + "http://dblp.uni-trier.de/" +  url.toString());
				links.add(url);
				inProceedings = false;
			}*/
			if(qName.equalsIgnoreCase("ee")){
				WebURL url = new WebURL();
				url.setURL(content.toString());
				//System.out.println("inProceedings: " + url.toString());
				links.add(url);
				inProceedings = false;
			}
		}
		if(inArticle){
			if(qName.equalsIgnoreCase("ee")){
				WebURL url = new WebURL();
				url.setURL(content.toString());
			//	System.out.println("inArticle - ee: " +  url.toString());
				links.add(url);
				//inArticle = false;
			}
			if(qName.equalsIgnoreCase("url")){
				WebURL url = new WebURL();
				url.setURL("http://dblp.uni-trier.de/" + content.toString());
		//		System.out.println("inArticle - url: " + "http://dblp.uni-trier.de/" + url.toString());
				links.add(url);
				inArticle = false;
			}
		}
		
	}
}
