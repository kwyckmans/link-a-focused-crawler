package ua.dbproject.searchengine;

import java.io.IOException;
import java.util.List;

import ua.dbproject.monitoring.AbstractSearchEngineMonitor;

public abstract class AbstractSearchEngine {

  protected AbstractSearchEngineMonitor searchEngineMonitor;

  // Initializes the search engine with an AbstractSearchEngineMonitor object.
  public void initialize(AbstractSearchEngineMonitor monitor) {
    this.searchEngineMonitor = monitor;
  }

  public abstract void start() throws IOException;

  public abstract void stop();

  public abstract List<PDFRecord> searchPDF(String searchText) throws IOException;
}
