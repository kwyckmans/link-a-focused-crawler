package ua.dbproject.searchengine;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SearchEngineServlet
 */
@WebServlet("/SearchEngineServlet")
public class SearchEngineServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private static final String CLASSIFIER_NAME = "MainClassifier";
	private static SearchEngine fSearchEngine;
	
    /**
     * Default constructor. 
     * @throws ServletException 
     */
    public SearchEngineServlet() throws ServletException {
        fSearchEngine = SearchEngine.getInstance(CLASSIFIER_NAME);
        try {
			fSearchEngine.start();
		} catch (IOException e) {
			throw new ServletException(e);
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Get get parameter for correct method
		String action = request.getParameter("action");
		response.setContentType("text/plain");   
		response.setCharacterEncoding("UTF-8");
		PrintWriter writer = response.getWriter();
						
		// Search
		if (action.equals("search")) {
					
			// Get the searchtext
			String searchText = request.getParameter("searchText");
					
			// Get the records
			List<PDFRecord> records = fSearchEngine.searchPDF(searchText);
			
			// Check whether there are records
			if (records.size() > 0) {
			
				// Print output
				writer.write("<p>Found " + records.size() + " matches for <b>" + searchText + "</b>.</p>");
				
				// Print table structure
				writer.write("<table class=\"table_normal\"><tr class=\"table_normal_title\">");
				writer.write("<td>Text</td><td>PDF</td>");
				
				// Print output for each record
				for (int recordNr = 0; recordNr < records.size(); recordNr++) {
					PDFRecord record = records.get(recordNr);
					writer.write("<tr class=\"table_normal_row\">");
					writer.write("<td>" + record.getTextOutput() + "</td>");
					writer.write("<td><a href=\"" + record.getUrl() + "\">View</a></td>");
					writer.write("</tr>");
				}
							
				// Print table structure
				writer.write("</table>");
									
			} else {
				writer.write("<p>No matching papers found for <b>" + searchText + "</b>.</p>");
			} 	

		} else {
			writer.write("ERROR");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
