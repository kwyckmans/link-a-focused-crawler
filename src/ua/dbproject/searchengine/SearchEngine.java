package ua.dbproject.searchengine;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.WildcardQuery;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import ua.dbproject.storage.StorageManager;
import ua.dbproject.tools.stemmer.StemmerDriver;

public class SearchEngine extends AbstractSearchEngine {

  // SearchEngineImpl should be a singleton.
  // Only one instance can be obtained via the getInstance() method.
  private static SearchEngine searchEngineInstance = null;
  private IndexSearcher fIndexSearcher;
  private String fClassifierName;
  private StemmerDriver fStemmer;

  private SearchEngine(String classifierName) {
    fClassifierName = classifierName;
    fStemmer = new StemmerDriver();
  }

  public static SearchEngine getInstance(String classifierName) {
    if (searchEngineInstance == null) {
      searchEngineInstance = new SearchEngine(classifierName);
    }
    return searchEngineInstance;
  }
  
  public void createIndexSearcher() throws IOException{
	  File indexFile = new File("../eclipseApps/link-a-focused-crawler/" + fClassifierName + "-index");
	  Directory dir = FSDirectory.open(indexFile);
	  IndexReader reader = DirectoryReader.open(dir);
	  fIndexSearcher = new IndexSearcher(reader); 
  }

  public void start() throws IOException {
	  createIndexSearcher();
  }

  public void stop() {
    // ... to be implemented
  }

//Search for pdf's on searchText
public List<PDFRecord> searchPDF(String searchText) throws IOException {
	
	// Stem searchText
	String[] searchStemmedAll = fStemmer.stem(searchText);
	String searchStemmed = "*";
	for (int i = 0; i < searchStemmedAll.length; i++) {
		searchStemmed += searchStemmedAll[i];
		searchStemmed += "*";
	}
	
	// Set up queryparser
	Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_44);
	QueryParser parser = new QueryParser(Version.LUCENE_44, "pdfStemmed", analyzer);
	List<PDFRecord> result = new LinkedList<PDFRecord>();
	 
	// Make query
	BooleanQuery query = new BooleanQuery();
	query.add(new WildcardQuery(new Term("pdfStemmed", searchStemmed)), BooleanClause.Occur.MUST);
	int numResults = 1000;
	ScoreDoc[] hits = fIndexSearcher.search(query, numResults).scoreDocs;
	for (int i = 0; i < hits.length; i++) {
		
		// Get document
		Document doc = fIndexSearcher.doc(hits[i].doc);
		
		// Convert document to PDFRecord and generate textoutput
		PDFRecord record = new PDFRecord(doc.get("url"), generateTextOutput(doc.get("pdfString"), searchStemmedAll[0]));
		
		// Add record to list
		result.add(record);
	} 
	
	// Return list of records
	return result;
}

private String generateTextOutput(String pdfText, String searchText) {
	
	// Search for index of searchText inside pdfText
	int index = pdfText.indexOf(searchText);
	
	if (index != -1) {
		
		// Adjust if neceassry
		if (index < 40) {
			index = 40;
		}
		
		// Make substring in the neighbourhoud
		String subString = "... ";
		subString += pdfText.substring(index - 40, index);
		subString += "<b>";
		String subsubString = pdfText.substring(index, index + searchText.length());
		subsubString = subsubString.toUpperCase();
		subString += subsubString;
		subString  += "</b>";
		subString += pdfText.substring(index + searchText.length(), index + searchText.length() + 20);
		subString += " ...";
	
		// Return result
		return subString;
		
	} else {
		String subString = "... ";
		subString += pdfText.substring(0, 60);
		subString += " ...";
		return subString;
	}
}
  
  
}
