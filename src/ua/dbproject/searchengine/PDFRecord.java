package ua.dbproject.searchengine;

public class PDFRecord {

	private String fUrl;
	private String fTextOutput;
	
	public PDFRecord(String url, String textOutput) {
		fUrl = url;
		fTextOutput = textOutput;
	}
	
	public String getUrl() {
		return fUrl;
	}
	
	public String getTextOutput() {
		return fTextOutput;
	}

}
