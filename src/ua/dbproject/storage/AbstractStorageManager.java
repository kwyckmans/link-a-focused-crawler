package ua.dbproject.storage;

import ua.dbproject.document.AbstractDocument;

public abstract class AbstractStorageManager {

  // Stores a document after it has been processed (downloaded, parsed, and classified).
  // Check out how you can use Lucene to index and search over your documents:
  // http://lucene.apache.org/core/
  public abstract void storeDocument(AbstractDocument document) throws Exception;
  
}
