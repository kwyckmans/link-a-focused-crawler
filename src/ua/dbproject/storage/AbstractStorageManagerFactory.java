package ua.dbproject.storage;

public abstract class AbstractStorageManagerFactory {

  // Produces a new instance of type AbstractStorageManager.
  public abstract AbstractStorageManager createStorageManager(String indexName, boolean create) throws Exception;

}
