package ua.dbproject.storage;

public class StorageManagerFactory extends AbstractStorageManagerFactory {

	public AbstractStorageManager createStorageManager(String indexName, boolean create) throws Exception {
		return new StorageManager(indexName, create);
	}


}
