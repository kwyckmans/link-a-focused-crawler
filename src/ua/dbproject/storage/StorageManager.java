package ua.dbproject.storage;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import ua.dbproject.document.AbstractDocument;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.util.Version;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;

public class StorageManager extends AbstractStorageManager {
	
	private String fIndexName;
	private IndexWriter fIndexWriter = null;
	private List<String> fUrlList;
	
	public StorageManager(String indexName, boolean create) throws Exception {
		// Store index name
		fIndexName = indexName;
		fUrlList = new LinkedList<String>();
		
		// Create index if needed
		if (create) {
			createIndex();
		}
	}
	
	// Creates a new index
	public void createIndex() throws Exception {
		
		// Create the directory
		boolean create = true;
		File indexFile = new File(fIndexName);
		if (indexFile.exists() && indexFile.isDirectory()) {
			create = true;
		}
		
		// Configuration of analyser
		Directory dir = FSDirectory.open(indexFile);
		Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_44);
		IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_44, analyzer);
		
		// Set create mode
		if (create) {
			config.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
		}
		
		// Make index
		IndexWriter writer = new IndexWriter(dir, config);
		writer.commit();
		writer.close(true);
	}
	
	// Create an indexwriter
	private void createIndexWriter() throws Exception {
		
		// Open the index directory
		boolean create = true;
		File indexFile = new File(fIndexName);
		Directory dir = FSDirectory.open(indexFile);
		
		// Create writer
		Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_44);
		IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_44, analyzer);
		config.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);
		fIndexWriter = new IndexWriter(dir, config);	
	}
	
	// Close the indexwriter
	private void closeIndexWriter() throws IOException {
		fIndexWriter.close();
	}
	
	// Get the indexwriter
	private IndexWriter getIndexWriter() throws Exception {
		if (fIndexWriter == null) {
			createIndexWriter();
		}
		return fIndexWriter;
	}

	public void storeDocument(AbstractDocument document) throws Exception {
		
		// Check whether url already exists
		String url = document.getPage().getWebURL().getURL();
		if (!fUrlList.contains(url)) {
			
			// Make new document to add
			Document lucDocument = new Document();
			lucDocument.add(new StringField("pdfStemmed", document.getStemmedContents(), Field.Store.YES));
			lucDocument.add(new StringField("url", document.getPage().getWebURL().getURL(), Field.Store.YES));
			lucDocument.add(new StringField("pdfString", document.getContents(), Field.Store.YES));
			IndexWriter writer = getIndexWriter();
			writer.addDocument(lucDocument);
			writer.commit();
			
			// Update url list
			fUrlList.add(url);
		}
	}
}
