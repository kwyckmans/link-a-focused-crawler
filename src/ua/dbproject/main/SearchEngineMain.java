package ua.dbproject.main;

import ua.dbproject.monitoring.SearchEngineMonitorImpl;
import ua.dbproject.searchengine.SearchEngine;

public class SearchEngineMain {

  // Starts the search engine.
  // Keyword queries should be processed interactively with a user. 
  public static void main(String[] args) {

    SearchEngine searchEngine = SearchEngine.getInstance("TEST");
    SearchEngineMonitorImpl monitor = SearchEngineMonitorImpl.getInstance();

    // ... more to be implemented here

    searchEngine.initialize(monitor);
    //searchEngine.start();
  }

}
