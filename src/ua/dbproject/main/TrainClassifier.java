package ua.dbproject.main;

import java.io.IOException;
import java.text.ParseException;

import ua.dbproject.classifier.AbstractClassifier;
import ua.dbproject.classifier.Classifier;
import ua.dbproject.classifier.ClassifierFactory;

// Make a new classifier and train it
public class TrainClassifier {

		final static String CLASSIFIER_NAME = "MainClassifier";
		
		public static void main(String[] args) {
			
			/******** TRAINER SETTINGS */
			// Positive PDF files (located in directory './storage/traindata/')
			String[] positiveFiles = {"Positive1","Positive2","Positive3","Positive4","Positive5"};
			
			// Negative PDF files (located in directory './storage/traindata/')
			String[] negativeFiles = {"Negative1", "Negative2"};
			/***************************/
			
			// Train classifier
			try {
				
				// Make new classifier
				System.out.println("Training classifier '" + CLASSIFIER_NAME + "' ...");
				ClassifierFactory classifierFactory = new ClassifierFactory();
				AbstractClassifier classifier = classifierFactory.createClassifier(CLASSIFIER_NAME);
				
				// Train classifier
				classifier.trainClassifier(positiveFiles, negativeFiles);
				
				// Write classifier to file
				classifier.writeToFile();
				System.out.println("Training finished");
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

}
