
package ua.dbproject.main;

import java.io.ByteArrayInputStream;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;

import edu.uci.ics.crawler4j.url.WebURL;
import jnisvmlight.SVMLightInterface;
import ua.dbproject.searchengine.SearchEngine;
import ua.dbproject.storage.StorageManager;
import ua.dbproject.tools.dblpxmlparser.DblpXMLParser;

public class Test {

	public static void main(String[] args) {
		
		// Test database
		try {
			// Store
			/*
			StorageManagerImpl stmng = new StorageManagerImpl();
			System.out.println("Index created");
			stmng.testAdd("www.papers.com", "Albert Test");
			stmng.testAdd("www.test.be", "Test");
			stmng.testAdd("www.test.be", "Albertje");
			stmng.testAdd("www.test.be", "Test Albert");
			stmng.closeIndexWriter();
			System.out.println("Document added");
			
			// Search
			SearchEngineImpl searchEngine = SearchEngineImpl.getInstance();
			searchEngine.start();
			searchEngine.testSearch("Albert");
			searchEngine.stop();*/
			//SVMLightInterface test = new SVMLightInterface();
			
			// XML Parser
			SAXParserFactory factory = SAXParserFactory.newInstance();
			DblpXMLParser handler = new DblpXMLParser();
			SAXParser saxParser = factory.newSAXParser();
			
			System.out.println("Parsing");
			saxParser.parse(new InputSource(new ByteArrayInputStream("<dblp><article key=\"journals/corr/HartatiU13\" publtype=\"informal publication\" mdate=\"2013-07-01\"><author>Sri Hartati</author><author>Shofwatul 'Uyun</author><title>Computation of Diet Composition for Patients Suffering from Kidney and Urinary Tract Diseases with the Fuzzy Genetic System.</title><year>2013</year><journal>CoRR</journal><ee>http://arxiv.org/abs/1306.5960</ee><volume>abs/1306.5960</volume><url>db/journals/corr/corr1306.html#HartatiU13</url></article></dblp>".getBytes("UTF-8"))), handler);
			
			System.out.println("Getting Links");
			List<WebURL> links = handler.getLinks();
			for(WebURL link:links){
				System.out.println(link.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}

}
