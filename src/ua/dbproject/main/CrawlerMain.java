package ua.dbproject.main;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;
import edu.uci.ics.crawler4j.url.WebURL;
import ua.dbproject.crawler.AbstractCrawler;
import ua.dbproject.crawler.CrawlerImpl;
import ua.dbproject.crawler.Worm;
import ua.dbproject.monitoring.CrawlerMonitorImpl;

public class CrawlerMain {
	static Logger logger = Logger.getLogger(CrawlerMain.class);
	
  // Starts the entire crawling process.
  public static void main(String[] args) {
	
    //CrawlerImpl crawler = CrawlerImpl.getInstance();
    CrawlerMonitorImpl monitor = CrawlerMonitorImpl.getInstance();

    // ... more to be implemented here
    
 // Create a configuration for a crawler
    CrawlConfig config = new CrawlConfig();
    
    // Location for temporary storage
    config.setCrawlStorageFolder("./storage");
    
    // Delay between visiting a domain
  //  config.setPolitenessDelay(1000);
    
    // Maximum depth of the crawler
    config.setMaxDepthOfCrawling(4);
    
    // Maximum number of pages to fetch
    config.setMaxPagesToFetch(100000);
    
    config.setMaxTotalConnections(10000);
    
    // If you want to be able to pause the crawling
    config.setResumableCrawling(false);
    config.setIncludeBinaryContentInCrawling(true);
    // User agent string to identify ourselves
    config.setUserAgentString("Link - A focused web crawler");
    
    config.setMaxDownloadSize(1048576*100);
    config.setConnectionTimeout(300000);
    config.setSocketTimeout(2000000);
    
    // TODO Figure out PageFetcher
    PageFetcher pageFetcher = new PageFetcher(config);
    
    // Configuration of Robots.txt
    RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
    RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
    
  // BasicConfigurator.configure();
   
    try {
    	// Create a controller for all worms. TODO CrawlerImpl should extend this
		AbstractCrawler crawler = CrawlerImpl.getInstance(config, pageFetcher, robotstxtServer);
		crawler.initialize(monitor);
		
		//crawler.start("http://dblp.uni-trier.de/db/indices/AUTHORS");
		crawler.start("http://dblp.uni-trier.de/rec/bibtex/journals/corr/cs-DB-0112007.xml");
		
		// We will always start crawling from the following URL:
	    // (but be polite - make sure you load at most 5 pages per minute from the same host!) 
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
  }
}
